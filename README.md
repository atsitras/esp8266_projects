# ESP8266_projects




#Code collection of my esp8266 projects.
- Multiple_reed_switches.ino. It reads from 5 different reed switches every 50 ms. Once there is a trigger, it sends it's status to MQTT server
- DHT11_NODE_RED_MQTT.ino. Through NODE RED it creates a web page to see Temp and Humidity. Also sends messages to MQTT server
- DHT11_Temp_Humidity_Webserver.ino. It creates a web server on the IP of the esp8266 and it shows info about Temperature (C) and Humidity.
- DHT11_telegram.ino. It queries esp8266 via Telegram and it shows info about Temperature (C) and Humidity.

- Reed_switch_telegram.ino. It reads from a reed switch. Once there is a status update it sends a message to the Telegram bot and to MQTT server.
    - [] For the Telegram part you need to create a bot and get the IDs.
    --[] Libraries:
        ---- Download from https://github.com/witnessmenow/Universal-Arduino-Telegram-Bot/archive/master.zip  the Universal Arduino Telegram Bot library.
        ---- Go to Sketch > Include Library > Add.ZIP Library... 

        ---- Go to Skech > Include Library > Manage Libraries.
        ---- Search for “ArduinoJson”.
        ---- Install the library.
    -[] Pin used is GPIO 4 (D2 in ESP8266), Ground and 3.3v. 








































########################################################################################
- https://cheatography.com/mrgrauel/cheat-sheets/gitlab-flavored-markdown/
- https://docs.gitlab.com/ee/user/markdown.html