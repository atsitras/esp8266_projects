//Multiple reed switches

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define WLAN_SSID       "XXXXXX"
#define WLAN_PASS       "YYYYYYYYYYY"

WiFiClient client;
PubSubClient mqtt(client, "192.168.160.240", 1883);

#define SENSOR_COUNT 5
#define DEVICE_NAME "alarm"

byte sensorPins[SENSOR_COUNT] = {5, 4, 14, 12, 13}; // The pins to which the sensors are connected.
boolean sensorStates[SENSOR_COUNT] = {1, 1, 1, 1, 1};
String deviceName = DEVICE_NAME;
/*String sensorNames[SENSOR_COUNT] = {"hallway","bedroom","livingroom","kidsroom","door"}; */
String sensorNames[SENSOR_COUNT] = {"hallway","door","livingroom","bedroom","kidsroom"};

void setup() {

    // the the pinmode for all used pins to INPUT_PULLUP
    for (byte i = 0; i < SENSOR_COUNT; i++) {
        byte pin = sensorPins[i];
        pinMode(pin, INPUT_PULLUP);
    }

    // Initiate the serial connection for debugging.
    Serial.begin(115200);
    delay(10);

    Serial.println("Starting ...");
}


void loop() {

    // Check if the Wifi is connected, if not, call the WiFi_connect() method.
    if (WiFi.status() != WL_CONNECTED) {
        WiFi_connect();
    }

    // Check if there is a connection with the MQTT server, if not, call the MQTT_connect() method.
    if (!mqtt.connected()) {
        Serial.println("MQTT Disconnected!");
        MQTT_connect();
    }

    // Call the loop method, to periodically ping the server to keep the connection alive.
    mqtt.loop();

    // Loop thru all the sensors defined at the start.
    for (byte i = 0; i < SENSOR_COUNT; i++) {

        // Create some helper variables.
        byte pin = sensorPins[i];
        boolean pinState = digitalRead(pin);
        boolean lastPinState =  sensorStates[i];
        String sensorName = sensorNames[i];

        // Check if the state for the current pin has changed.
        if (pinState != lastPinState) {

            // Define a string with the topic to which we want to post the new state to ...
            // and convert it into a char * array which we need for the pubsubclient.
            String feedNameString =  String("/device/" + deviceName + "/sensor/" + sensorName);
            char topic[feedNameString.length() + 1];
            feedNameString.toCharArray(topic, feedNameString.length() + 1);

            // Output the new state to the serial for debugging.
            Serial.print("New state for sensor ");
            Serial.print(topic);
            Serial.print(": ");
            Serial.print(pinState);

            // Publish the new state to the MQTT server.
            // The message is sent as a retained message to always
            // have the last state available on the broker.
            // The QoS is set to 1, to make sure the delivery
            // of the mseeage to the broker is guaranteed.
            if (mqtt.publish(MQTT::Publish(topic, pinState ? "1" : "0").set_retain().set_qos(1).set_dup())) {
                Serial.println(F(" ... Published!"));
            } else {
                Serial.println(F(" ... MQTT PUBLISH FAILED!"));
            }

            // Store the new pinstate in the pinStates array.
            sensorStates[i] = pinState;
        }
    }

    // Wait 50 miliseconds before we repeat the runloop.
    delay(50);
}


void WiFi_connect() {
    // Print some debuggin info to the serial terminal.
    Serial.println(); Serial.println();
    Serial.print("Connecting to ");
    Serial.print(WLAN_SSID);
    Serial.print(" ");

    // Connect to the WiFi netwerk using the previously defined SSID and Password.
    WiFi.begin(WLAN_SSID, WLAN_PASS);
    while (WiFi.status() != WL_CONNECTED) {
        // If there is no connection. Wait half a second before checking again.
        delay(500);
        Serial.print(".");
    }
    Serial.println();

    // After the connection is established, print the IP-adres to the serial terminal for debugging.
    Serial.print("WiFi connected! - ");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
}

void MQTT_connect() {
    // Define a string with the topic to which we want to post the connection state of this device to ...
    // and convert it into a char * array which we need for the pubsubclient.
    String feedNameString =  String("/device/" + deviceName + "/connected");
    char topic[feedNameString.length() + 1];
    feedNameString.toCharArray(topic, feedNameString.length() + 1);

    // As long as there is no active MQTT connection ...
    while (!mqtt.connected()) {
        Serial.print("Attempting MQTT connection ... ");

        // Try to connect to the MQTT broker.
        // As the 'will' we set the topic and connection state to 0.
        // This means that is if the connection is dropped,
        // the broker will publish the connection state to the defined topic.
        if (mqtt.connect(MQTT::Connect(DEVICE_NAME).set_will(topic, "0", 1, true))) {
            Serial.println("Connected!");

            // After the connection is established, publish a new connection state.
            mqtt.publish(MQTT::Publish(topic, "1").set_retain().set_qos(1));

        } else {

            // Wait half a second before trying again.
            Serial.println("Failed! - Try again in half a second.");
            delay(500);
        }
    }
}